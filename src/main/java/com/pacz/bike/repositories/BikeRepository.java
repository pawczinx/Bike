package com.pacz.bike.repositories;

import com.pacz.bike.models.Bike;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikeRepository extends JpaRepository <Bike, Long> {

}
